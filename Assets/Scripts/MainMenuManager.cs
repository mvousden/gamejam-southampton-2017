﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement; //Allows you to open scenes
using UnityEngine;
using UnityEditor;


public class MainMenuManager : MonoBehaviour {

	public void Load()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void Quit()
    {
        // Uncomment before build: Application.Quit();
        EditorApplication.isPlaying = false;
    }
}
