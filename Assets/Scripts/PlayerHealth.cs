﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//jamin
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    float startingHealth = 100;
    float healthReward = 50; /* Amount the maximum health increases by when a fire is returned. */
    float maxHealth;
    public Slider fireSlider;
    Collider2D other;
    public float currentHealth;

    double decayRate = 5;  /* Health decay per second. */
    double restoreRate = 50;
    float mistMultiplier = 2; /* Increased rate of death when in mist. */
    bool grounded = true;
    //GameObject FoxAsset;
    PlayerMovement playerMovement;
    GameOverManager gameOverManager;
    Animator anim;
    public Animator whiteAnim;

    //jamins code : 
    //bool isAlive = true;
    public float deathTimeOut;
    // GameObject child = GameObject.Find("DeathObject");
    float ableToHeal;
    // Audio management
    List<AudioSource> audioSourcesList = new List<AudioSource>();
    float volumeCap;
    bool deadSoundPlayed = false;

    // Use this for initialization
    void Awake()
    {
        GetComponents<AudioSource>(audioSourcesList);    /* See PlayerMovement.cs for list of sources. */
        volumeCap = 0.2f;
        currentHealth = startingHealth;
        maxHealth = startingHealth;
        fireSlider.value = currentHealth;
        playerMovement = GetComponent<PlayerMovement>();
        anim = this.GetComponent<Animator>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //OnCollisionEnter2D (other);
        if (grounded == true)
        {
            // playerMovement.OnTriggerEnter2D(other);
            InvokeRepeating("TakeDamage", 0f, 600f);

        }

        //Deathcheat
        if (Input.GetKeyDown(KeyCode.P))
        {
            currentHealth += 10;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            currentHealth -= 10;
        }


        //jamins stuff
        if (currentHealth <= 0)
        {
            Debug.Log("Restart");
            //isAlive = false;
            //print("timewait 5");
            //space for death sprite
            //destroy the object
            //Destroy(this.gameObject);
            whiteAnim.SetTrigger("GameOver");
            this.GetComponent<PlayerMovement>().enabled = false;
            //this.GetComponentInChildren<SpriteRenderer>().enabled = true;
            foreach (Renderer r in GetComponentsInChildren<SpriteRenderer>())
                r.enabled = true;
            this.GetComponent<SpriteRenderer>().enabled = false;
            // child.gameObject.SetActive(true);
            //transform.GetChild(0).gameObject.SetActive(true);
            //wait a time amount    
            //StartCoroutine(TimeDelay());
            //call fucntion to reset the level
            Invoke("restartTheLevel", 3);
        }
    }


    //jamins stuff
    void restartTheLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnCollisionEnter2D(Collision2D collision) // once object collides with ground
    {
        TakeDamage();
    }

    void TakeDamage()
    {
        float healthDelta;

        if (currentHealth > 0)
        {
            if (playerMovement.atHome == false)
            {
                healthDelta = (float)decayRate;
            }
            else
            {
                healthDelta = -(float)restoreRate;
            }

            if (playerMovement.inMist == true)
            {
                healthDelta *= mistMultiplier;
            }

            /* deltaTime is the time the previous frame took to execute.
                 * Not ideal, but pretty good. */
            currentHealth -= healthDelta * UnityEngine.Time.deltaTime;

            if (currentHealth > maxHealth) { currentHealth = maxHealth; }
        }

        UpdateMusic();
        UpdateHealthBar();
    }

    public void FullHealth()
    /* Health refilled when at home fire. */
    {
        currentHealth = maxHealth;
        UpdateHealthBar();
    }

    public void OnFireCollect()
    /* When we collect a fire, recharge health by 50%. */
    {
        currentHealth += 0.5f * maxHealth;

        /* Don't exceed the maximum health. */
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        UpdateHealthBar();
    }

    public void OnFireDeposit()
    /* When we deposit a fire, increase maximum health, and recharge health. */
    {
        maxHealth += healthReward;
        FullHealth();
    }

    void UpdateHealthBar()
    /* Updates the health bar with the player's current and maximum health. */
    //jamins bit

    {
        fireSlider.value = currentHealth / maxHealth;  /* Scales between 0 and 1. */
        RectTransform myt = fireSlider.GetComponent<RectTransform>();
        myt.sizeDelta = new Vector2(maxHealth, 40.0f);
    }
    void UpdateMusic()
    /* Updates music based on player's health. */
    {
        float relativeHealth;
        relativeHealth = currentHealth / maxHealth;

        if (relativeHealth > 0.5)
        {
            audioSourcesList[2].volume = volumeCap;
            audioSourcesList[3].volume = 0;
        } else if (relativeHealth > 0.25)
        {
            audioSourcesList[2].volume = relativeHealth * 2 * volumeCap;
            audioSourcesList[3].volume = (1 - (relativeHealth * 2)) * volumeCap * 6;
        }
        else /* about to die. */
        {
            audioSourcesList[2].volume = relativeHealth * 2 * volumeCap;
            audioSourcesList[3].volume = relativeHealth * 2 * volumeCap * 6;
        }

        /* Game over tune. */
        if (relativeHealth <= 0 && deadSoundPlayed == false)
        {
            audioSourcesList[2].Stop();
            audioSourcesList[3].Stop();
            audioSourcesList[4].Play();
            deadSoundPlayed = true;
        }
    }
}
