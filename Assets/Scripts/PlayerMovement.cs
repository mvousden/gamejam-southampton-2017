﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {
	Rigidbody2D rb;
	public float speed;
	public float jumpHeight;
	public bool grounded = true;
    public bool ground = false; // ground variable
    //GameObject FoxAsset;
    PlayerHealth playerHealth;
    public int score = 0;
    bool isHeld = false;
    public bool atHome = false;
    public bool inMist = false;

	//Animation related objects
	private Animator foxAnimator;

    // Something about jumping
    public bool beginjump=false;
    public float jumplimit = 60.0f;
    public float jump = 0.0f;

    // Audio management
    List<AudioSource> audioSourcesList = new List<AudioSource>();

    // Use this for initialization
    void Start () {

        GetComponents<AudioSource>(audioSourcesList);        


        /* Audio sources in order:
         *  - 0: Torch pickup sound
         *  - 1: Torch dropoff sound
         *  - 2: Cheerful background music
         *  - 3: Ominous background music
         *  - 4: The fox is dead */ 

        rb = GetComponent<Rigidbody2D> ();
		Application.targetFrameRate = 60;
		Time.maximumDeltaTime = 0.0f;
        playerHealth = GetComponent<PlayerHealth>();

		//Initialize animation
		foxAnimator = this.GetComponent<Animator>();

	}

	void FixedUpdate(){

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
			transform.Translate (new Vector2 (speed, 0) * Time.deltaTime);
			foxAnimator.SetInteger ("direction", 1);
			foxAnimator.SetBool ("isMoving", true);
		}

		if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow)) {
			foxAnimator.SetBool ("isMoving", false);
		}

		if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow)) {
			foxAnimator.SetBool ("isMoving", false);
		}

		if (Input.GetKey (KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {

			transform.Translate (new Vector2 (-speed, 0) * Time.deltaTime);
			foxAnimator.SetInteger ("direction", 0);
			foxAnimator.SetBool ("isMoving", true);
		}	

				
        if ((Input.GetKeyUp(KeyCode.Space)) && (beginjump == true)) // use up key to jump (my version to let you understand how it works)
        {
            beginjump = false;
            jump = 0;
        }
        if (((Input.GetKey(KeyCode.Space)) ) && (beginjump == true)) // use up key to jump (my version to let you understand how it works)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpHeight)*3);
            jump += 1;
            if (jump >= jumplimit)
            {
                beginjump = false;
                jump = 0;
            }
            foxAnimator.SetBool("isJumping", true);
        }

		if ((Input.GetKeyDown(KeyCode.Space) ) && (ground == true)) // use up key to jump (my version to let you understand how it works)
        {
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpHeight) * 70);
			
            beginjump = true;
        }
			
    }

	public void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.CompareTag("Fire"))
        {
            if (isHeld == true)
            {
                return;
            }
            else
            {
                other.gameObject.SetActive(false);
                isHeld = true;
                audioSourcesList[0].Play();                
                playerHealth.OnFireCollect();
				foxAnimator.SetBool ("holdFire", true);
            }
        }
        else if (other.gameObject.CompareTag("HomeFire"))
        {
            atHome = true;

            if ((ScoreManager.score == 5) && (isHeld = true))
            {
                //Application.LoadLevel("NextLevelPlaceholder");
                //SceneManager.UnloadScene("Tutorial");
                ScoreManager.score = 0;
                SceneManager.LoadScene("Level1");
            } else if(isHeld == true)
            {
                ScoreManager.score += 1;
				foxAnimator.SetBool ("holdFire", false);
                audioSourcesList[1].Play();
                playerHealth.OnFireDeposit();
                isHeld = false;
            }
        else if (other.gameObject.tag == "Mist")
            {
                inMist = true;
            }
        }
			
		//if (other.gameObject.CompareTag ("Floor")) {
			//grounded = true;
		//} else {
		//	grounded = false;
		//}
	}

    void OnCollisionEnter2D(Collision2D collision) // once object collides with ground
    {
		if ((collision.gameObject.tag == "Floor")) {
			ground = true;
            //
			foxAnimator.SetBool ("isJumping", false);
		}
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HomeFire")
        {
            atHome = false;
        }
        if (collision.gameObject.tag == "Mist")
        {
            inMist = false;
        }
    }

    //jamin fix thanks to zac
    void OnCollisionStay2D(Collision2D collision)
    {
        if ((collision.gameObject.tag == "Floor"))
        {
            ground = true;
            //
            foxAnimator.SetBool("isJumping", false);
        }
    }

    void OnCollisionExit2D(Collision2D collision) // once object is no longer colliding with ground
    {
        if (collision.gameObject.tag == "Floor")
        {
            ground = false;

            
            //
            //foxAnimator.SetBool("isJumping", true);

        }
    }

}
	
