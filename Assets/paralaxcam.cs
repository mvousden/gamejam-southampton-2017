﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paralaxxcam : MonoBehaviour
{

    public GameObject player;

    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position;
    }

    // Update is called once per frame but guaranteed to run after objects run in update. Definitely player moved.
    void LateUpdate()
    {
        transform.position = player.transform.position + offset; //moves cam to new position as if it was child
    }
}
