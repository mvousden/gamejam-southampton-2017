Homefire (this Unity project) is our entry for Gamejam 2017 in Southampton, which we made in one 48-hour sprint!

Check out v0.1 for the version we posted at the showcase. Download the entry at https://homefire.itch.io/homefire

Contributors, in no particular order:

    - Benjamin Went
    - Emma Waugh
    - Peter Ashdown
    - Mark Vousden
    - Sarah Ryan
    - Ivan Ling
    - Michael Cooper
    - Alex Marshall
    - Ben Woolford
    - Mike Clark
